package eu.bagins.weatherdemo.model

import com.google.gson.annotations.SerializedName

data class Location (
    @SerializedName("name") val name: String,
    @SerializedName("country") val country: String,
    @SerializedName("lat") val latitude: Double,
    @SerializedName("lon") val longitude: Double,
)