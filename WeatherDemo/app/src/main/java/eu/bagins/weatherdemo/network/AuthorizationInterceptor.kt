package eu.bagins.weatherdemo.network

import java.io.IOException
import okhttp3.Interceptor
import okhttp3.Response


class AuthorizationInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url
            .newBuilder()
            .addQueryParameter("appid", APPID)
            .build()
        val request = chain.request().newBuilder().url(url).build()
        return chain.proceed(request)
    }

    companion object {

        const val APPID = "85bf635dbc34d3b017fa5cad5fdd5ac2"
    }
}
