package eu.bagins.weatherdemo.hilt

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.location.LocationManager
import android.util.Log
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.network.AuthorizationInterceptor
import eu.bagins.weatherdemo.network.WeatherApi
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@InstallIn(SingletonComponent::class)
@Module
class GeneralModule {

    @Provides
    @Singleton
    internal fun provideRetrofitBuilder(okHttpClient: OkHttpClient, factory: Converter.Factory) = Retrofit.Builder()
        .baseUrl(API_URL)
        .addConverterFactory(factory)
        .client(okHttpClient)

    @Provides
    @Singleton
    fun provideRetrofitApi(builder: Retrofit.Builder): WeatherApi = builder.build().create(WeatherApi::class.java)

    @Provides
    fun provideCoroutineContextProvider() = CoroutineDispatcherProvider()

    @Provides
    @Singleton
    internal fun provideAuthorizationInterceptor() =
        AuthorizationInterceptor()

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        authorizationInterceptor: AuthorizationInterceptor,
        cache: Cache?
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .dispatcher(Dispatcher().apply {
                maxRequestsPerHost = MAX_REQUESTS
            })
            .connectTimeout(HTTP_CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(HTTP_CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)
            //.cache(cache)
        return builder.addInterceptor(authorizationInterceptor).addInterceptor(loggingInterceptor).build()
    }

    @Provides
    @Singleton
    internal fun provideCache(cacheDir: File): Cache? {
        var cache: Cache? = null
        try {
            cache = Cache(cacheDir, HTTP_CACHE_SIZE)
        } catch (e: Exception) {
            Log.e("GeneralModule", e.toString())
        }
        return cache
    }

    @Provides
    @Singleton
    internal fun provideCacheDir(coreApp: Application) = File(coreApp.cacheDir, HTTP_CACHE_DIR)

    @Provides
    @Singleton
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Provides
    @Singleton
    internal fun provideConverterFactory(): Converter.Factory {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.apply {
            setDateFormat(GSON_DATE_FORMAT)
        }
        return GsonConverterFactory.create(gsonBuilder.create())
    }

    @Provides
    fun provideSharedPreferences(application: Application) =
        application.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE)!!

    @Provides
    fun providePreferencesStorage(sharedPreferences: SharedPreferences) = PreferencesStorage(sharedPreferences)

    @Provides
    @Singleton
    internal fun provideLocationManager(app: Application) =
        app.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    companion object {

        private const val GSON_DATE_FORMAT =
            "yyyy-MM-dd'T'HH:mm:ssZ" //gson has bug in format parsing - requires separate date format (without ' character)
        private const val MAX_REQUESTS = 10
        private const val HTTP_CONNECTION_TIMEOUT = 60
        private const val API_URL = "https://api.openweathermap.org/"
        private const val HTTP_CACHE_DIR = "http-cache"
        private const val HTTP_CACHE_SIZE = (10 * 1024 * 1024).toLong() //10MB
        private const val KEY_PREFERENCES = "preferences"
    }
}