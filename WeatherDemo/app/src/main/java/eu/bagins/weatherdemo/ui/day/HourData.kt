package eu.bagins.weatherdemo.ui.day

import android.graphics.drawable.Drawable

data class HourData(
    val time: String,
    val temp: String,
    val humidity: String,
    val icon: Drawable?,
    val weekend: Boolean
)