package eu.bagins.weatherdemo.ui.settings

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.use
import com.google.android.material.card.MaterialCardView
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.ui.day.HourData

class TemperatureTile @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    init {
        var typedArray: TypedArray? = null
        attrs?.let {
            typedArray = context.obtainStyledAttributes(it, R.styleable.TempTile)
        }

        inflate(
            context,
            R.layout.temp_tile,
            this
        )

        typedArray?.use {
            findViewById<TextView>(R.id.time).text = it.getString(R.styleable.TempTile_time)
            findViewById<TextView>(R.id.temp).text = it.getString(R.styleable.TempTile_temp)
            findViewById<TextView>(R.id.humid).text = it.getString(R.styleable.TempTile_humid)
            findViewById<ImageView>(R.id.icon).setImageResource(
                it.getResourceId(
                    R.styleable.TempTile_icon,
                    0
                )
            )

            background = ColorDrawable(
                ContextCompat.getColor(
                    context,
                    if (it.getBoolean(
                            R.styleable.TempTile_extra,
                            false
                        )
                    ) R.color.primaryLightColor else R.color.secondaryLightColor
                )
            )
        }
        elevation = 0f
    }

    fun setData(data: HourData) {
        findViewById<TextView>(R.id.time).text = data.time
        findViewById<TextView>(R.id.temp).text = data.temp
        findViewById<TextView>(R.id.humid).text = data.humidity
        data.icon?.let {
            findViewById<ImageView>(R.id.icon).setImageDrawable(it)
        }
        findViewById<MaterialCardView>(R.id.root).backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(context, if (data.weekend) R.color.primaryColor else R.color.lightBackground)
        )
    }
}