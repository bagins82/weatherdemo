package eu.bagins.weatherdemo.ui

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelStoreOwner
import kotlin.reflect.KClass

abstract class BaseFragment<VM : ViewModel, B : ViewDataBinding> : Fragment() {

    protected val viewModel: VM by lazyViewModels()

    private var internalBinding: B? = null
    protected val binding get() = internalBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        internalBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        internalBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (backButtonAvailable()) {
            setHasOptionsMenu(true)
        }
    }

    override fun onStart() {
        super.onStart()
        if (backButtonAvailable()) {
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        if (!TextUtils.isEmpty(getTitleText())) {
            activity?.title = getTitleText()
        }
    }

    override fun onStop() {
        super.onStop()
        if (backButtonAvailable()) {
            (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    protected abstract fun getLayoutRes(): Int
    protected abstract fun getVMClass(): KClass<VM>
    protected open fun getViewModelStoreOwner(): ViewModelStoreOwner = requireActivity()

    protected open fun getTitleText(): String = ""
    open fun navigateBackwards() = false

    protected open fun backButtonAvailable() = false

    protected fun lazyViewModels() =
        ViewModelLazy(
            getVMClass(),
            { getViewModelStoreOwner().viewModelStore },
            { defaultViewModelProviderFactory }
        )

    protected inline fun <reified T : ViewModel> lazyActivityViewModels() =
        ViewModelLazy(
            T::class,
            { requireActivity().viewModelStore },
            { requireActivity().defaultViewModelProviderFactory }
        )
}
