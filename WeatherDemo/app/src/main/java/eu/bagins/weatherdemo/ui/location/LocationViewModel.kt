package eu.bagins.weatherdemo.ui.location

import android.app.Application
import android.location.LocationManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.bagins.weatherdemo.Event
import eu.bagins.weatherdemo.LocationHelper
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.model.BaseViewModel
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.network.WeatherApi
import javax.inject.Inject

@HiltViewModel
class LocationViewModel @Inject constructor(
    private val preferencesStorage: PreferencesStorage,
    private val weatherApi: WeatherApi,
    private val application: Application,
    private val locationManager: LocationManager,
    coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    private var yourLocation: LocationItemViewModel? = null
    private var downloadedLocations: List<LocationItemViewModel>? = null

    val locationText = MutableLiveData("")
    val locations = MutableLiveData<List<LocationItemViewModel>>()
    val showExplanation = MutableLiveData<Event<Any>>()
    val showLocationError = MutableLiveData<Event<Any>>()

    fun locationEntered() {
        Log.d("LocationChange", "Location ${locationText.value}")
        if (!locationText.value.isNullOrBlank()) {
            launchIOCoroutine {
                downloadedLocations = weatherApi.getCity(locationText.value as String, 5).map {
                    LocationItemViewModel(it.latitude.toFloat(), it.longitude.toFloat(), it.name, it.country, false)
                }
                withMainContext {
                    val yourLocationIfAvailable =
                        yourLocation?.let { listOf(yourLocation!!) } ?: listOf()
                    locations.value =
                        yourLocationIfAvailable + (downloadedLocations.orEmpty())
                }
            }
        }
    }

    private fun initLocation() {
        if (!LocationHelper(application, locationManager).updateCurrentLocation { lat, lon, city ->
                yourLocation = LocationItemViewModel(lat, lon, city, "", true)
                locations.value = listOf(yourLocation!!) + (downloadedLocations.orEmpty())
            }) {
            showLocationError.value = Event(0)
        }
    }

    fun updateCurrentLocation() {
        initLocation()
    }

    fun showPermissionExplanation() {
        showExplanation.value = Event(0)
    }

    fun locationSelected(location: LocationItemViewModel) {
        preferencesStorage.saveCity(location.city)
        preferencesStorage.saveLocation(location.latitude, location.longitude)
        preferencesStorage.saveUseCurrentLocation(location.currentLocation)
    }
}