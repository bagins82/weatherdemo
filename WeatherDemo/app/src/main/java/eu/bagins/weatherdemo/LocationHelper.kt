package eu.bagins.weatherdemo

import android.annotation.SuppressLint
import android.app.Application
import android.location.Geocoder
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log
import java.util.Locale

class LocationHelper(val application: Application, val locationManager: LocationManager) {

    @SuppressLint("MissingPermission")
    fun updateCurrentLocation(locationListener: (Float, Float, String) -> Unit): Boolean {
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ||
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        ) {
            Log.d("Location", "lastLocation = null")
            val locationListener = object : LocationListener {
                override fun onLocationChanged(location: android.location.Location) {
                    locationManager.removeUpdates(this)
                    Log.d("Location", "currentLocation = $location")
                    try {
                        val addresses = Geocoder(application, Locale.getDefault()).getFromLocation(
                            location.latitude,
                            location.longitude,
                            1
                        )

                        val cityName =
                            if (addresses[0].locality.isNullOrBlank()) addresses[0].subLocality else addresses[0].locality
                        locationListener(location.latitude.toFloat(), location.longitude.toFloat(), cityName)
                    } catch (e: Exception) {
                        Log.d("Geolocation", "Error: $e")
                        //ignore errors for now
                    }
                }
            }
            locationManager.requestLocationUpdates(LocationManager.FUSED_PROVIDER, 0, 0f, locationListener)
            return true
        } else {
            return false
        }
    }
}