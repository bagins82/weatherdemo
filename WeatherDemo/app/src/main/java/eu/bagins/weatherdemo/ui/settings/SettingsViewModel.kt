package eu.bagins.weatherdemo.ui.settings

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.bagins.weatherdemo.Event
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.model.BaseViewModel
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.ui.settings.SettingsFragment.Companion.DAY_SETTINGS_NUM
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val preferencesStorage: PreferencesStorage, coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    val isCelcius = MutableLiveData<Boolean>()
    val workDays = (0 until DAY_SETTINGS_NUM).map { MutableLiveData<Int>() }
    val weekends = (0 until DAY_SETTINGS_NUM).map { MutableLiveData<Int>() }

    val notUniqueHour = MutableLiveData<Event<Any>>()

    fun initItems() {
        isCelcius.value = preferencesStorage.getTemperatureUnit()
        workDays.forEachIndexed { index, workday ->
            workday.value = preferencesStorage.getWorkdayHour(index)
        }
        weekends.forEachIndexed { index, weekend ->
            weekend.value = preferencesStorage.getWeekendHour(index)
        }
    }

    fun saveWorkday(index: Int, workday: Int) {
        val prev = preferencesStorage.getWorkdayHour(index)
        if (checkWorkdayHoursUnique()) {
            preferencesStorage.saveWorkdayHour(index, workday)
        } else {
            workDays[index].value = prev
            notUniqueHour.value = Event(0)
        }
    }

    fun saveWeekend(index: Int, workday: Int) {
        val prev = preferencesStorage.getWeekendHour(index)
        if (checkWeekendHoursUnique()) {
            preferencesStorage.saveWeekendHour(index, workday)
        } else {
            weekends[index].value = prev
            notUniqueHour.value = Event(0)
        }
    }

    fun triggerDataRefresh() {
        preferencesStorage.getWeather()?.let {
            preferencesStorage.saveWeatherJsonString(it)
        }
    }

    private fun checkWorkdayHoursUnique() = workDays.map { it.value }.distinct().size == DAY_SETTINGS_NUM

    private fun checkWeekendHoursUnique() = weekends.map { it.value }.distinct().size == DAY_SETTINGS_NUM
}