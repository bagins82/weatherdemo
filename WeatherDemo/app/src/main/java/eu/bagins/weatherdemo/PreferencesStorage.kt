package eu.bagins.weatherdemo

import android.content.SharedPreferences
import androidx.lifecycle.MediatorLiveData
import com.google.gson.Gson
import eu.bagins.weatherdemo.model.OneCallReadout

class PreferencesStorage(private val prefs: SharedPreferences) {

    fun saveCity(city: String) {
        prefs.edit().run {
            putString(KEY_CITY, city)
            apply()
        }
    }

    fun useCurrentLocation() = prefs.getBoolean(KEY_CURRENT_LOCATION, true)

    fun saveUseCurrentLocation(useCurrent: Boolean) =
        prefs.edit().putBoolean(KEY_CURRENT_LOCATION, useCurrent).apply()

    fun getCityLiveData() = prefs.stringLiveData(KEY_CITY, INIT_CITY)

    fun clearCity() {
        prefs.edit().run {
            remove(KEY_CITY)
            apply()
        }
    }

    fun saveLocation(latitude: Float, longitude: Float) {
        prefs.edit().run {
            putFloat(KEY_LATITUDE, latitude)
            putFloat(KEY_LONGITUDE, longitude)
            apply()
        }
    }

    fun getLocation(): Pair<Float, Float> {
        return Pair(prefs.getFloat(KEY_LATITUDE, 0f), prefs.getFloat(KEY_LONGITUDE, 0f))
    }

    fun getLocationLiveData() = MediatorLiveData<Pair<Float, Float>>().apply {
        var longitude: Float? = null
        var latitude: Float? = null

        fun update() {
            if (latitude != null && longitude != null) {
                value = Pair(latitude!!, longitude!!)
            }
        }

        addSource(prefs.floatLiveData(KEY_LATITUDE, INIT_LAT)) {
            latitude = it
            update()
        }

        addSource(prefs.floatLiveData(KEY_LONGITUDE, INIT_LON)) {
            longitude = it
            update()
        }
    }

    fun saveWeather(weather: OneCallReadout) {
        saveWeatherJsonString(Gson().toJson(weather))
    }

    fun saveWeatherJsonString(json: String) {
        prefs.edit().putString(KEY_WEATHER, json).apply()
    }

    fun getWeatherLiveData() = prefs.stringLiveData(KEY_WEATHER, "")

    fun getWeather() = prefs.getString(KEY_WEATHER, "")

    fun getTemperatureUnit() = prefs.getBoolean(TEMPERATURE_UNIT_CELCIUS, true)
    fun saveTemperatureUnit(celcius: Boolean) = prefs.edit().putBoolean(TEMPERATURE_UNIT_CELCIUS, celcius).apply()

    fun getWorkdayHour(index: Int) = prefs.getInt(WORKDAY_ + index.toString(), INIT_WORKDAY_HORUS[index])
    fun saveWorkdayHour(index: Int, hour: Int) = prefs.edit().putInt(WORKDAY_ + index.toString(), hour).apply()

    fun getWeekendHour(index: Int) = prefs.getInt(WEEKEND_ + index.toString(), INIT_WEEKEND_HORUS[index])
    fun saveWeekendHour(index: Int, hour: Int) = prefs.edit().putInt(WEEKEND_ + index.toString(), hour).apply()

    companion object {

        private const val KEY_CURRENT_LOCATION = "key_current_location"
        private const val KEY_LONGITUDE = "key_longitude"
        private const val KEY_LATITUDE = "key_latitude"
        private const val KEY_CITY = "key_city"
        private const val KEY_WEATHER = "key_weather"
        private const val TEMPERATURE_UNIT_CELCIUS = "temperature_unit"

        private const val WORKDAY_ = "workday_"
        private const val WEEKEND_ = "weekend_"

        private val INIT_WORKDAY_HORUS = listOf(8, 13, 17, 20)
        private val INIT_WEEKEND_HORUS = listOf(10, 15, 18, 21)

        private val INIT_LAT = 51.5285578f
        private val INIT_LON = -0.2420227f
        private val INIT_CITY = "London"
    }
}