package eu.bagins.weatherdemo.ui.location

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import eu.bagins.weatherdemo.Event
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.LocationItemBinding

class LocationAdapter : ListAdapter<LocationItemViewModel, LocationAdapter.LocationViewHolder>(DiffCallback) {

    object DiffCallback : DiffUtil.ItemCallback<LocationItemViewModel>() {

        override fun areItemsTheSame(oldItem: LocationItemViewModel, newItem: LocationItemViewModel) =
            oldItem.latitude == newItem.latitude && oldItem.longitude == newItem.longitude

        override fun areContentsTheSame(oldItem: LocationItemViewModel, newItem: LocationItemViewModel) =
            oldItem.city == newItem.city && oldItem.country == newItem.country && oldItem.currentLocation == newItem.currentLocation
    }

    val itemSelected = MutableLiveData<Event<LocationItemViewModel>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder =
        LocationViewHolder(LocationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) = holder.bind(getItem(position))

    inner class LocationViewHolder(val binding: LocationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(locationItem: LocationItemViewModel) {
            with(binding) {
                item = locationItem
                binding.root.setOnClickListener {
                    itemSelected.value = Event(locationItem)
                }
                binding.cardView.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(binding.root.context, locationItem.getBackgroundColor())
                )
                executePendingBindings()
            }
        }
    }
}

open class LocationItemViewModel(
    val latitude: Float,
    val longitude: Float,
    val city: String,
    val country: String,
    val currentLocation: Boolean,
) : ViewModel() {

    fun getBackgroundColor() = if (currentLocation) R.color.secondaryColor else android.R.color.transparent
}