package eu.bagins.weatherdemo.ui.day

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.FragmentWeatherBinding
import eu.bagins.weatherdemo.ui.BaseFragment
import kotlin.reflect.KClass

@AndroidEntryPoint
class WeatherFragment : BaseFragment<WeatherViewModel, FragmentWeatherBinding>() {

    override fun getLayoutRes() = R.layout.fragment_weather

    override fun getVMClass(): KClass<WeatherViewModel> = WeatherViewModel::class

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        viewModel.currentRotation.observe(viewLifecycleOwner) {
            binding.windNowDir.rotation = it
        }

        registerObservers()
        return root
    }


    private fun registerObservers() {
        viewModel.locationName.observe(viewLifecycleOwner) {
            (requireActivity() as AppCompatActivity).supportActionBar?.apply {
                title = it
            }
        }

        viewModel.getWeatherLiveData().observe(viewLifecycleOwner) {
            viewModel.weatherReceived(it)
        }
    }

    override fun onStart() {
        super.onStart()
        setToolbar()
    }

    private fun setToolbar() {
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            title = viewModel.locationName.value
        }
        requireActivity().invalidateOptionsMenu()
    }
}