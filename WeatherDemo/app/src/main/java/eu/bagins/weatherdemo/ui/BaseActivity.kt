package eu.bagins.weatherdemo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import kotlin.reflect.KClass

abstract class BaseActivity<VM : ViewModel, B : ViewDataBinding> : AppCompatActivity() {

    protected val viewModel: VM by lazyViewModels()

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutRes())
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this

        title = getTitleText()
    }

    protected abstract fun getLayoutRes(): Int
    protected abstract fun getVMClass(): KClass<VM>

    protected open fun getTitleText(): String = ""

    private fun lazyViewModels() =
        ViewModelLazy(getVMClass(), { viewModelStore }, { defaultViewModelProviderFactory })

    protected inline fun <reified T : ViewModel> lazyActivityViewModels() =
        ViewModelLazy(
            T::class,
            { viewModelStore },
            { defaultViewModelProviderFactory }
        )
}