package eu.bagins.weatherdemo.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.FragmentSettingsBinding
import eu.bagins.weatherdemo.ui.BaseFragment
import kotlin.reflect.KClass

@AndroidEntryPoint
class SettingsFragment : BaseFragment<SettingsViewModel, FragmentSettingsBinding>() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)
        registerObservers()
        viewModel.initItems()
        return root
    }

    private fun registerObservers() {
        viewModel.isCelcius.observe(viewLifecycleOwner) {
            binding.unitsRadioGroup.check(if (it) R.id.celcius else R.id.fahrenheit)
        }
        (0 until DAY_SETTINGS_NUM).forEach { index ->
            viewModel.workDays[index].observe(viewLifecycleOwner) {
                viewModel.saveWorkday(index, it)
            }
        }
        (0 until DAY_SETTINGS_NUM).forEach { index ->
            viewModel.weekends[index].observe(viewLifecycleOwner) {
                viewModel.saveWeekend(index, it)
            }
        }
        viewModel.notUniqueHour.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let {
                Snackbar.make(
                    requireContext(),
                    binding.root,
                    getString(R.string.not_unique_hour),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        setToolbar()
    }

    override fun onStop() {
        viewModel.triggerDataRefresh()
        super.onStop()
    }

    private fun setToolbar() {
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setTitle(R.string.settings)
        }
        requireActivity().invalidateOptionsMenu()
    }

    override fun getLayoutRes() = R.layout.fragment_settings

    override fun getVMClass(): KClass<SettingsViewModel> = SettingsViewModel::class

    companion object {

        const val DAY_SETTINGS_NUM = 4
    }
}