package eu.bagins.weatherdemo.ui.week

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import eu.bagins.weatherdemo.Event
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.WeekDayItemBinding

class WeekAdapter : ListAdapter<WeekDayItemViewModel, WeekAdapter.WeekDayViewHolder>(DiffCallback) {

    object DiffCallback : DiffUtil.ItemCallback<WeekDayItemViewModel>() {

        override fun areItemsTheSame(oldItem: WeekDayItemViewModel, newItem: WeekDayItemViewModel) =
            oldItem.day == newItem.day

        override fun areContentsTheSame(oldItem: WeekDayItemViewModel, newItem: WeekDayItemViewModel) =
            oldItem.day == newItem.day &&
                    oldItem.weekend == newItem.weekend &&
                    oldItem.icon == newItem.icon &&
                    oldItem.temp == newItem.temp &&
                    oldItem.tempNight == newItem.tempNight &&
                    oldItem.wind == newItem.wind &&
                    oldItem.windDegree == newItem.windDegree &&
                    oldItem.clouds == newItem.clouds &&
                    oldItem.humidity == newItem.humidity
    }

    val itemSelected = MutableLiveData<Event<WeekDayItemViewModel>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekDayViewHolder =
        WeekDayViewHolder(WeekDayItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: WeekDayViewHolder, position: Int) = holder.bind(getItem(position))

    inner class WeekDayViewHolder(val binding: WeekDayItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(weekDayItem: WeekDayItemViewModel) {
            with(binding) {
                item = weekDayItem
                binding.windDir.rotation = weekDayItem.windDegree
                binding.icon.setImageDrawable(ContextCompat.getDrawable(binding.root.context, weekDayItem.icon))
                binding.root.setOnClickListener {
                    itemSelected.value = Event(weekDayItem)
                }
                binding.day.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(binding.root.context, weekDayItem.getBackgroundColor())
                )
                executePendingBindings()
            }
        }
    }
}

open class WeekDayItemViewModel(
    val day: String,
    val icon: Int,
    val temp: String,
    val tempNight: String,
    val wind: String,
    val windDegree: Float,
    val clouds: String,
    val humidity: String,
    val weekend: Boolean,
) : ViewModel() {

    fun getBackgroundColor() = if (weekend) R.color.primaryColor else android.R.color.transparent
}