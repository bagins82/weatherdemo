package eu.bagins.weatherdemo

import android.content.Context
import android.content.pm.PackageManager
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class ActivityPermissionsHandler(
    private val activity: AppCompatActivity,
    private val onPermissionsGranted: () -> Unit,
    private val showPermissionsExplanation: () -> Unit,
    private val onPermissionsDenied: () -> Unit
) {

    private var requestPermissionsLauncher: ActivityResultLauncher<Array<String>>? = null

    init {
        requestPermissionsLauncher = activity.registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            if (results.isNotEmpty() && results.all { (_, isGranted) -> isGranted }) {
                onPermissionsGranted()
            } else {
                onPermissionsDenied()
            }
        }
    }

    fun checkPermissions(permissions: Array<String>) {
        checkPermissionsInternal(
            permissions,
            requestPermissionsLauncher!!,
            onPermissionsGranted,
            showPermissionsExplanation,
            activity,
        ) { permission ->
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                permission
            )
        }
    }

    fun requestPermissions(permissions: Array<String>) {
        requestPermissionsLauncher!!.launch(permissions)
    }

    private fun checkPermissionsInternal(
        permissions: Array<String>,
        requestPermissionsLauncher: ActivityResultLauncher<Array<String>>,
        onPermissionsGranted: () -> Unit,
        showPermissionsExplanation: () -> Unit,
        context: Context,
        shouldShowRequestPermissionRationale: (String) -> Boolean
    ) {
        val permissionsMapping = permissions.map { permission ->
            permission to (ContextCompat.checkSelfPermission(
                context,
                permission
            ) == PackageManager.PERMISSION_GRANTED)
        }
        if (permissionsMapping.any { (_, isGranted) -> !isGranted }) {
            val notGranted =
                permissionsMapping.filter { (_, isGranted) -> !isGranted }.map { (permission, _) -> permission }
            if (notGranted.any { shouldShowRequestPermissionRationale(it) }) {
                showPermissionsExplanation()
            } else {
                requestPermissionsLauncher.launch(notGranted.toTypedArray())
            }
        } else {
            onPermissionsGranted()
        }
    }
}