package eu.bagins.weatherdemo.ui.day

import android.app.Application
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.getImageIcon
import eu.bagins.weatherdemo.isWeekend
import eu.bagins.weatherdemo.model.BaseViewModel
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.model.OneCallReadout
import eu.bagins.weatherdemo.toHour
import eu.bagins.weatherdemo.ui.settings.SettingsFragment.Companion.DAY_SETTINGS_NUM
import java.time.Instant
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val preferencesStorage: PreferencesStorage,
    private val application: Application,
    coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    val locationName = preferencesStorage.getCityLiveData()

    val currentTemp = MutableLiveData<String>()
    val currentWindSpeed = MutableLiveData<String>()
    val currentRotation = MutableLiveData<Float>()
    val currentClouds = MutableLiveData<String>()
    val currentHumidity = MutableLiveData<String>()
    val currentIcon = MutableLiveData<Drawable>()
    val hoursData = MutableLiveData((0 until DAY_SETTINGS_NUM * 2).map { HourData("", "", "", null, false) })

    fun getWeatherLiveData() = preferencesStorage.getWeatherLiveData()

    fun weatherReceived(it: String) {
        val readout = Gson().fromJson(it, OneCallReadout::class.java)
        displayReadout(readout)
    }

    private fun displayReadout(readout: OneCallReadout?) {
        if (readout != null) {
            currentTemp.value = application.getString(R.string.celcius, readout.current.temp)
            currentIcon.value =
                ContextCompat.getDrawable(application, readout.current.weather.first().icon.getImageIcon())
            currentWindSpeed.value = application.getString(R.string.mpers, readout.current.windSpeed)
            currentRotation.value = readout.current.windDegree
            currentClouds.value = application.getString(R.string.percent, readout.current.clouds)
            currentHumidity.value = application.getString(R.string.percent, readout.current.humidity)

            val normalHours = (0 until DAY_SETTINGS_NUM).map { preferencesStorage.getWorkdayHour(it) }

            val weekendHours = (0 until DAY_SETTINGS_NUM).map { preferencesStorage.getWeekendHour(it) }

            val hours = mutableListOf<HourData>()
            readout.hourly.forEach { hour ->
                val matchHours = if (hour.datetime.isWeekend()) weekendHours else normalHours
                if (matchHours.contains(hour.datetime.toHour()) && hours.size < DAY_SETTINGS_NUM * 2) {
                    hours.add(
                        HourData(
                            application.getString(R.string.hour_format, hour.datetime.toHour()),
                            application.getString(R.string.celcius, hour.temp),
                            application.getString(R.string.percent, hour.humidity),
                            ContextCompat.getDrawable(application, hour.weather.first().icon.getImageIcon())!!,
                            hour.datetime.isWeekend()
                        )
                    )
                }
            }
            //in some hours setup (for openweathermap - it might be not possible to find specific hour - fill it in with rest
            fillInMissingHourReadouts(hours)
            hoursData.value = hours
        } else {
            hoursData.value = (0 until (DAY_SETTINGS_NUM * 2)).map { HourData("", "", "", null, false) }
        }
    }

    private fun fillInMissingHourReadouts(hours: MutableList<HourData>) {
        if (hours.size < DAY_SETTINGS_NUM * 2) {
            (0 until (DAY_SETTINGS_NUM * 2 - hours.size)).forEach {
                hours.add(
                    HourData(
                        application.getString(R.string.na),
                        application.getString(R.string.na),
                        application.getString(R.string.na),
                        null,
                        false
                    )
                )
            }
        }
    }
}
