package eu.bagins.weatherdemo.ui.week

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.FragmentWeekBinding
import eu.bagins.weatherdemo.ui.BaseFragment
import kotlin.reflect.KClass

@AndroidEntryPoint
class WeekFragment : BaseFragment<WeekViewModel, FragmentWeekBinding>() {

    override fun getLayoutRes() = R.layout.fragment_week

    override fun getVMClass(): KClass<WeekViewModel> = WeekViewModel::class

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)
        binding.weekList.layoutManager = LinearLayoutManager(requireContext())
        binding.weekList.adapter = WeekAdapter()
        viewModel.getWeekData().observe(viewLifecycleOwner) {
            (binding.weekList.adapter as WeekAdapter).submitList(it)
        }

        registerObservers()
        return root
    }


    private fun registerObservers() {
        viewModel.locationName.observe(viewLifecycleOwner) {
            (requireActivity() as AppCompatActivity).supportActionBar?.apply {
                title = it
            }
        }
    }

    override fun onStart() {
        super.onStart()
        setToolbar()
    }

    private fun setToolbar() {
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            title = viewModel.locationName.value
        }
        requireActivity().invalidateOptionsMenu()
    }
}