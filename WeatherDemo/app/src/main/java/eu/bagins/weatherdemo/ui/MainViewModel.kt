package eu.bagins.weatherdemo.ui

import android.annotation.SuppressLint
import android.app.Application
import android.location.LocationManager
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.bagins.weatherdemo.Event
import eu.bagins.weatherdemo.LocationHelper
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.model.BaseViewModel
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.model.OneCallReadout
import eu.bagins.weatherdemo.network.WeatherApi
import javax.inject.Inject
import kotlinx.coroutines.delay

@HiltViewModel
class MainViewModel @Inject constructor(
    private val weatherApi: WeatherApi,
    private val locationManager: LocationManager,
    private val preferencesStorage: PreferencesStorage,
    private val application: Application,
    coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    val showExplanation = MutableLiveData<Event<Any>>()
    val showLocationError = MutableLiveData<Event<Any>>()
    val refreshWeather = MutableLiveData<Event<Boolean>>()

    @SuppressLint("MissingPermission")
    fun updateCurrentLocation() {
        if (!LocationHelper(application, locationManager).updateCurrentLocation { lat, lon, city ->
                preferencesStorage.saveLocation(lat, lon)
                preferencesStorage.saveCity(city)
            }) {
            showLocationError.value = Event(0)
        }
    }

    fun refreshWeather(latitude: Float?, longitude: Float?, weatherDownloaded: (OneCallReadout?) -> Unit) {
        val loc = preferencesStorage.getLocation()
        Log.d("Weather", "location $loc")
        launchIOCoroutine {
            try {
                Log.d("Weather", "download")
                val weather = weatherApi.oneCallForecast(latitude ?: loc.first, longitude ?: loc.second)
                delay(1000) //user will be able to see progress spinner
                Log.d("Weather", "downloaded")
                preferencesStorage.saveWeather(weather)
                weatherDownloaded(weather)
            } catch (e: Exception) {
                Log.d("CommunicationError", e.toString())
                weatherDownloaded(null)
            }
        }
    }

    fun showPermissionExplanation() {
        showExplanation.value = Event(0)
    }

    fun getWeatherLiveData() =
        Transformations.map(preferencesStorage.getWeatherLiveData()) { weatherJson ->
            Gson().fromJson(weatherJson, OneCallReadout::class.java)
        }

    fun getLocationLiveData() =
        preferencesStorage.getLocationLiveData()

    fun getInitialWeather() {
        refreshWeather.value = Event(preferencesStorage.useCurrentLocation())
    }
}