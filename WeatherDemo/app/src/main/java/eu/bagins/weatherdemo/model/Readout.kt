package eu.bagins.weatherdemo.model

import com.google.gson.annotations.SerializedName

data class OneCallReadout(
    @SerializedName("current") val current: OneCallSingleReadout,
    @SerializedName("hourly") val hourly: List<OneCallSingleReadout>,
    @SerializedName("daily") val daily: List<DailyReadout>,
)

data class DailyReadout(
    @SerializedName("dt") val datetime: Long,
    @SerializedName("temp") val temp: TempDailyReadout,
    @SerializedName("weather") val weather: List<WeatherReadout>,
    @SerializedName("wind_speed") val windSpeed: Float,
    @SerializedName("wind_deg") val windDegree: Float,
    @SerializedName("pressure") val pressure: Float,
    @SerializedName("humidity") val humidity: Float,
    @SerializedName("clouds") val clouds: Float,
)

data class TempDailyReadout(
    @SerializedName("day") val day: Float,
    @SerializedName("night") val night: Float,
)

data class OneCallSingleReadout(
    @SerializedName("dt") val datetime: Long,
    @SerializedName("temp") val temp: Float,
    @SerializedName("feels_like") val feel: Float,
    @SerializedName("pressure") val pressure: Float,
    @SerializedName("humidity") val humidity: Float,
    @SerializedName("clouds") val clouds: Float,
    @SerializedName("wind_speed") val windSpeed: Float,
    @SerializedName("wind_gust") val windGust: Float,
    @SerializedName("wind_deg") val windDegree: Float,
    @SerializedName("weather") val weather: List<WeatherReadout>,
)

data class WeatherReadout(
    @SerializedName("icon") val icon: String,
)