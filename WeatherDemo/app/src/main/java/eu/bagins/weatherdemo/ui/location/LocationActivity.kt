package eu.bagins.weatherdemo.ui.location

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import eu.bagins.weatherdemo.ActivityPermissionsHandler
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.databinding.ActivityLocationBinding
import eu.bagins.weatherdemo.hideKeyboard
import eu.bagins.weatherdemo.ui.BaseActivity
import kotlin.reflect.KClass

@AndroidEntryPoint
class LocationActivity : BaseActivity<LocationViewModel, ActivityLocationBinding>() {

    private val permissionsHandler = ActivityPermissionsHandler(this,
        onPermissionsGranted = {
            viewModel.updateCurrentLocation()
        },
        showPermissionsExplanation = {
            viewModel.showPermissionExplanation()
        },
        onPermissionsDenied = {
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.location)
        binding.locationList.adapter = LocationAdapter()
        binding.locationList.layoutManager = LinearLayoutManager(this)
        binding.location.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == IME_ACTION_DONE && viewModel.locationText.value!!.isNotEmpty()) {
                hideKeyboard(binding.location)
                viewModel.locationEntered()
                true
            } else {
                false
            }
        }

        registerObservers()
        permissionsHandler.checkPermissions(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    private fun registerObservers() {
        viewModel.locations.observe(this) {
            (binding.locationList.adapter as LocationAdapter).submitList(it)
        }

        viewModel.showExplanation.observe(this) {
            it.getContentIfNotHandled()?.let {
                showLocationPermissionExplanation()
            }
        }

        viewModel.showLocationError.observe(this) {
            showLocationError()
        }

        (binding.locationList.adapter as LocationAdapter).itemSelected.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                viewModel.locationSelected(it)
                finish()
            }
        }
    }

    private fun showLocationError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.could_not_get_location)
            .setMessage(R.string.could_not_get_location_tet)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .create()
            .show()
    }

    private fun showLocationPermissionExplanation() {
        AlertDialog.Builder(this)
            .setTitle(R.string.location_permission_required)
            .setMessage(R.string.location_permission_required_text)
            .setPositiveButton(R.string.ok) { _, _ ->
                permissionsHandler.requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                )
            }
            .create()
            .show()
    }

    override fun getLayoutRes() = R.layout.activity_location

    override fun getVMClass(): KClass<LocationViewModel> = LocationViewModel::class
}
