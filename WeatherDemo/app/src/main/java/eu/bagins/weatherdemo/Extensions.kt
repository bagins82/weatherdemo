package eu.bagins.weatherdemo

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.BindingAdapter
import eu.bagins.weatherdemo.ui.settings.TemperatureTile
import eu.bagins.weatherdemo.ui.day.HourData
import java.time.DayOfWeek
import java.time.Instant
import java.time.ZoneId
import java.time.format.TextStyle
import java.util.Locale

fun Activity.hideKeyboard(focusedView: View) {
    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(focusedView.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
}

fun String.getImageIcon() =
    when (this) {
        "01d" -> R.drawable.w01d
        "01n" -> R.drawable.w01n
        "02d" -> R.drawable.w02d
        "02n" -> R.drawable.w02n
        "03d" -> R.drawable.w03d
        "03n" -> R.drawable.w03n
        "04d" -> R.drawable.w04d
        "04n" -> R.drawable.w04n
        "09d" -> R.drawable.w09d
        "09n" -> R.drawable.w09n
        "10d" -> R.drawable.w10d
        "10n" -> R.drawable.w10n
        "11d" -> R.drawable.w11d
        "11n" -> R.drawable.w11n
        "13d" -> R.drawable.w13d
        "13n" -> R.drawable.w13n
        "50d" -> R.drawable.w50d
        "50n" -> R.drawable.w50n
        else -> error("Unknown icon!")
    }

fun Long.toHour() = Instant.ofEpochSecond(this).atZone(ZoneId.systemDefault()).hour

fun Long.isWeekend(): Boolean {
    val day = Instant.ofEpochSecond(this).atZone(ZoneId.systemDefault()).dayOfWeek
    return day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY
}

fun Long.dayName(): String =
    Instant.ofEpochSecond(this).atZone(ZoneId.systemDefault()).dayOfWeek.getDisplayName(
        TextStyle.FULL,
        Locale.getDefault()
    )

@BindingAdapter("hourData")
fun TemperatureTile.setHourData(hourData: HourData) {
    this.setData(hourData)
}