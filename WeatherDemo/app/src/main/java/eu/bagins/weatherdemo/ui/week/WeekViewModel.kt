package eu.bagins.weatherdemo.ui.week

import android.app.Application
import androidx.lifecycle.Transformations
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.bagins.weatherdemo.PreferencesStorage
import eu.bagins.weatherdemo.R
import eu.bagins.weatherdemo.dayName
import eu.bagins.weatherdemo.getImageIcon
import eu.bagins.weatherdemo.isWeekend
import eu.bagins.weatherdemo.model.BaseViewModel
import eu.bagins.weatherdemo.model.CoroutineDispatcherProvider
import eu.bagins.weatherdemo.model.OneCallReadout
import javax.inject.Inject

@HiltViewModel
class WeekViewModel @Inject constructor(
    private val preferencesStorage: PreferencesStorage,
    private val application: Application,
    coroutineDispatcherProvider: CoroutineDispatcherProvider
) : BaseViewModel(coroutineDispatcherProvider) {

    val locationName = preferencesStorage.getCityLiveData()

    fun getWeekData() = Transformations.map(preferencesStorage.getWeatherLiveData()) { weatherJson ->
        val weather = Gson().fromJson(weatherJson, OneCallReadout::class.java)
        weather.daily.map {
            WeekDayItemViewModel(
                it.datetime.dayName(),
                it.weather.first().icon.getImageIcon(),
                application.getString(R.string.celcius, it.temp.day),
                application.getString(R.string.celcius, it.temp.night),
                application.getString(R.string.mpers, it.windSpeed),
                it.windDegree,
                application.getString(R.string.percent, it.clouds),
                application.getString(R.string.percent, it.humidity),
                it.datetime.isWeekend(),
            )
        }
    }
}