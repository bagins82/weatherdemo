package eu.bagins.weatherdemo.model

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

open class BaseViewModel(coroutineDispatcherProvider: CoroutineDispatcherProvider) : ViewModel() {

    private val viewModelJob = SupervisorJob()
    private val ioScope = CoroutineScope(coroutineDispatcherProvider.IO + viewModelJob)

    private val viewModelMainJob = SupervisorJob()
    private val mainScope = CoroutineScope(coroutineDispatcherProvider.Main + viewModelMainJob)

    override fun onCleared() {
        viewModelJob.cancel()
        viewModelMainJob.cancel()
        super.onCleared()
    }

    protected fun launchIOCoroutine(function: suspend () -> Unit) =
        ioScope.launch { function() }

    protected fun launchMainCoroutine(function: suspend () -> Unit) =
        mainScope.launch { function() }

    protected fun <T> asyncIO(function: suspend () -> T): Deferred<T> =
        ioScope.async { function() }

    protected suspend fun withMainContext(function: suspend () -> Unit) =
        withContext(mainScope.coroutineContext) {
            function()
        }

    protected suspend fun <T> withIOContext(function: suspend () -> T) =
        withContext(ioScope.coroutineContext) {
            function()
        }
}