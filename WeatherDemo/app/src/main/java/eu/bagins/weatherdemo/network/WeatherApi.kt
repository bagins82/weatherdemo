package eu.bagins.weatherdemo.network

import eu.bagins.weatherdemo.model.Location
import eu.bagins.weatherdemo.model.OneCallReadout
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("data/2.5/onecall")
    suspend fun oneCallForecast(
        @Query("lat") latitude: Float,
        @Query("lon") longitude: Float,
        @Query("exclude") exclude: String = "minutely,alerts",
        @Query("units") units: String = "metric",
    ): OneCallReadout

    @GET("geo/1.0/direct")
    suspend fun getCity(
        @Query("q") query: String,
        @Query("limit") limit: Int,
    ): List<Location>
}