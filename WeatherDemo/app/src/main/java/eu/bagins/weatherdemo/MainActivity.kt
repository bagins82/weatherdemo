package eu.bagins.weatherdemo

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import eu.bagins.weatherdemo.databinding.ActivityMainBinding
import eu.bagins.weatherdemo.ui.BaseActivity
import eu.bagins.weatherdemo.ui.MainViewModel
import eu.bagins.weatherdemo.ui.location.LocationActivity
import kotlin.reflect.KClass

@AndroidEntryPoint
class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    private val permissionsHandler = ActivityPermissionsHandler(this,
        onPermissionsGranted = {
            viewModel.updateCurrentLocation()
        },
        showPermissionsExplanation = {
            viewModel.showPermissionExplanation()
        },
        onPermissionsDenied = {
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        navView.setupWithNavController(navController)

        binding.refresh.setOnRefreshListener {
            Log.d("MainActivity", "pullToRefresh")
            refreshWeather()
        }

        registerObservers()
        viewModel.getInitialWeather()
    }

    private fun registerObservers() {
        viewModel.refreshWeather.observe(this) { event ->
            event.getContentIfNotHandled()?.let { currentLocation ->
                if (currentLocation) {
                    permissionsHandler.checkPermissions(
                        arrayOf(
                            ACCESS_FINE_LOCATION,
                            ACCESS_COARSE_LOCATION
                        )
                    )
                } else {
                    refreshWeather()
                }
            }
        }

        viewModel.showLocationError.observe(this) {
            showLocationError()
        }

        viewModel.showExplanation.observe(this) {
            it.getContentIfNotHandled()?.let {
                showLocationPermissionExplanation()
            }
        }

        viewModel.getLocationLiveData().observe(this) { (latitude, longitude) ->
            Log.d("MainActivity", "refresh by location change")
            refreshWeather(latitude, longitude)
        }
    }

    private fun showLocationError() {
        AlertDialog.Builder(this)
            .setTitle(R.string.could_not_get_location)
            .setMessage(R.string.could_not_get_location_tet)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .create()
            .show()
    }

    private fun showLocationPermissionExplanation() {
        AlertDialog.Builder(this)
            .setTitle(R.string.location_permission_required)
            .setMessage(R.string.location_permission_required_text)
            .setPositiveButton(R.string.ok) { _, _ ->
                permissionsHandler.requestPermissions(
                    arrayOf(
                        ACCESS_FINE_LOCATION,
                        ACCESS_COARSE_LOCATION
                    )
                )
            }
            .create()
            .show()
    }

    private fun refreshWeather(latitude: Float? = null, longitude: Float? = null) {
        Log.d("Refresh", "refresh $latitude $longitude")
        binding.refresh.isRefreshing = true
        viewModel.refreshWeather(latitude, longitude) {
            Log.d("Refresh", "refresh finished success = ${it != null}")
            if (it == null) {
                Snackbar.make(binding.navView, R.string.could_not_download_weather, Snackbar.LENGTH_LONG).show()
            }
            binding.refresh.isRefreshing = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        menu!!.findItem(R.id.location).isVisible =
            navController.currentDestination?.id == R.id.navigation_weather                || navController.currentDestination?.id == R.id.navigation_week
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        startActivity(Intent(this, LocationActivity::class.java))
        return true
    }

    override fun getLayoutRes() = R.layout.activity_main

    override fun getVMClass(): KClass<MainViewModel> = MainViewModel::class
}