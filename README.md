# WeatherDemo application (eu.bagins.weatherdemo)

## Application description

Main assumption of the application was to present weather data customized for user.
Application consists of 3 main tabs:
1. Weather tab
2. Week tab
3. Settings tab
4. Location view

### Tabs description

1. Weather tab
	Consist of 3 sections:
	* current weather 
	* weather for next 24h (weekend hours are marked with different color)
	* weather for another 24h (weekend hours are marked with different color)
	Weather is displayed not for whole 24h hours but for selected 4 important hours (user can specify those hours in settings tab)
2. Week tab
	Shows general weather for upcoming 8 days.
3. Settings tab
	Allows to set 4 important user hours (for example: leave home time, going for lunch time, going back from work time) for work days.
	Allows to set 4 important user hours for weekend days.
4. Location view
	Location view allows user select location to display weather for:
	* current user location (if user enables location permission) - always display weather for user current location
	* selected user location - user can search for city and save it - always display weather for user selected location
5. User can 'pull to refresh' data on every tab
	
## Implementation

### Used features/libraries

Application was implemented for android devices using:

1. MVVM Architecture

2. HILT for dependency injetion

3. Retrofit to handle api communication

4. Simple SharedPreferences to store data (no need to user ROOM DB or other solution for such simple usage)

5. All icons used in application are material design icons.

For fetching weather data **openweathermap.org** api was used.

## Limitations

### Api/Time limitations/Tradeoffs

Due to time limit, some areas of the application could not be implemented the way they should be:

1. Settings options should be implemented by using android Preferences classes. However they are difficult to customize - so options were implemented by using simple fragment

2. Settings option contains switch to select temperature units. However it does not work (not enough time to finish it) 

3. **openweathermap.org** has some limitations: searching for location (latitude, longitude) base on location name displays only 5 results and user should enter full location name.

4. For now all the view elements are not clickable - probably it would be good to allow user to click on specific tile to display more details about weather for specific hour or day. This would patch the problem that user can see only hours he has selected in settings.

5. It would be good to allow user to change more unit types (for example wind in m/s or km/h)

6. For sure app contains errors that were not found/no time to fix.	(for example - hour data only provides data for next 48 hours - so there might be situation where app won't find 8 measurements for next 48 hours)

## Repository

### Bitbucket repository content

Repository contains:

1. Source code of the application ready to compile in Android Studio

2. Compiled **WeatherDemo.apk** (debug version) file, ready to be installed on android devices (from Android 8.0)
